#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar 18 23:23:04 2022

@author: neel
"""

import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate

#1. de Sitter approximation to inflation, instantaneous reheating
# Inflation is assumed to be de Sitter throughout, and the radiation dominated
# era is taken to start immediately after its end. Thus, the Hubble scale of
# inflation is determined by the reheating temperature.

M_pl = 1
GeV = M_pl/(2.4*(10**18))
eV = GeV/(10**9)
T_re = (100)*GeV
T_0 = eV*2.73/11604

def H(T):
    # assuming 100 relativistic degrees of freedom. T is the temperature in
    # the radiation dominated era
    return 2*T*T*np.sqrt(4*100*np.pi*np.pi/(90*M_pl*M_pl))

# Here, the convention is a_re = 1. Hence, comoving numbers have to be divided by a_0
# to get the physical wavenumber at the present time.
a_re = 1
a_0 = (T_re/T_0)*(100/7.25)**(1/3)
H_I = H(T_re)

def a_inf(eta):
    if eta <= 0:
        return 1/(1 - H_I*eta)
    else:
        raise ValueError(f"Only conformal time <= 0 works for now. Conformal time = 0 at end of inflation. Conformal time={eta} encountered.")

def ainf_etaeta(eta):
    if eta <= 0:
        return 2*H_I*H_I*(1 - H_I*eta)**(-2)
    else:
        raise ValueError(f"Only conformal time <= 0 works for now. Conformal time = 0 at end of inflation. Conformal time={eta} encountered.")

k_f = a_re*H_I
H_Iinv = 1/H_I

def F(eta, u_vec, k):
    return np.array([u_vec[1],(ainf_etaeta(eta) - k**2)*u_vec[0]])

def scipy_int(k):
    # Integrates from BD initial conditions to find u_k at the end of inflation.
    # If k is >> a*H even at the end of inflation, it is simply given the BD form
    # at the end of inflation and no numerical integration is done.
    if k/k_f < 1000:
        init_eta = H_Iinv - 1000/k # Give BD initial conditions when k = 1000*a*H
        u_init = np.exp(-1j*k*init_eta)/np.sqrt(2*k)
        uinit_eta = (-1j*k)*u_init
        #deta = -init_eta/300000 # put this as max_step in solve_ivp if needed
        ans = scipy.integrate.solve_ivp(F,(init_eta,0),np.array([u_init,uinit_eta]),args=(k,),atol=0)
        return ans
    else:
        return 1/np.sqrt(2*k)

def P_T_scipy(k):
    if k/k_f < 1000:
        ans = scipy_int(k)
        return (2*k**3*((np.abs(ans.y/a_re)**2)/(np.pi*np.pi)))[0][-1]
    else:
        return (2*k**3*((np.abs(scipy_int(k)/a_re)**2)/(np.pi*np.pi)))

k_arr = [0.000001*k_f, 0.0001*k_f, 0.01*k_f, k_f, 100*k_f, 10000*k_f, 1000000*k_f]
P_T_arr = []

for k in k_arr:
    P_T_arr.append(P_T_scipy(k))

plt.figure(1)
plt.loglog()
plt.plot(k_arr/k_f, P_T_arr/(H_I*H_I/(np.pi*np.pi*M_pl*M_pl)))
plt.xlabel("k/k_f")
plt.ylabel("$\mathcal{P}_T(k)$ in units of $H_I^2$/$\pi^2M_pl^2$")


# Below is an attempt to write an RK4 integration routine manually for the
# numerical integration without using scipy. It has bugs and can be ignored for now.

# u_arr = []
# eta_arr = []
# t_arr = []
# ainf_arr = []

# def P_T(k):
#     k_arr = np.array([k])
#     a = []
#     for i in k_arr:
#         init_eta = H_Iinv - 1000/i #initialize when k/aH = 1000
#         #print(init_eta)
#         u_init = np.exp(-1j*i*init_eta)/np.sqrt(2*i)
#         uinit_eta = (-1j*i)*u_init
#         u = u_init
#         u_eta = uinit_eta
#         #print(u)
#         deta = -init_eta/1000000
#         eta = init_eta
#         u_arr.append(u)
#         eta_arr.append(eta)
#         for t in range(1000000-1):
#             f1 = deta*(ainf_etaeta(eta) - i**2)*u
#             g1 = deta*u_eta
#             f2 = deta*(ainf_etaeta(eta + 0.5*deta) - i**2)*(u + 0.5*g1)
#             g2 = deta*(u_eta + 0.5*f1)
#             f3 = deta*(ainf_etaeta(eta + 0.5*deta) - i**2)*(u + 0.5*g2)
#             g3 = deta*(u_eta + 0.5*f2)
#             f4 = deta*(ainf_etaeta(eta + deta) - i**2)*(u + g3)
#             g4 = deta*(u_eta + f3)
#             u_eta = u_eta + (f1 + 2*f2 + 2*f3 + f4)/6
#             u = u + (g1 + 2*g2 + 2*g3 + g4)/6
#             u_arr.append(u)
#             eta = eta + deta
#             eta_arr.append(eta)
#             ainf_arr.append(a_inf(eta))
#             if i*H_Iinv/a_inf(eta) < 10 and i*H_Iinv/a_inf(eta) > 0.1:
#                 t_arr.append(t)
#         #print(u)
#     return(2*k**3*((np.abs(u)/a_re)**2)/(np.pi*np.pi))